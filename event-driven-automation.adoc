= Event Driven Automation
Camry Fedei @cfedei
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._



*Use case:* Automating event response for configuration changes, security preparedness, or emergency incident handling across a scaling estate, whether on-premises in physical data centers or in public/private clouds.

*Background:* With the growing complexity of our modern infrastructures, the importance of automating our day-to-day operations to avoid the costly overhead spent on maintaining this complex environment is increasing rapidly. From something as simple as pushing a configuration change in response to changing security regulations, to something as serious as recognizing and responding to a security threat or emergency, it’s more important than ever that these responses happen as fast as possible, and are consistent and reliable across the entire organization. 

== Solution overview

====
*Why Event Driven Automation?*

. Automate the remediation of events from monitored hosts
. Scalable patching and security compliance updates
. Security and compliance enforcement and emergency response
====


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/eda-marketecture-slide.png[alt="An event driven architecture designed to improve emergency response while improving automation and management of a hybrid cloud environment", width=700]
--

== Summary video
video::W_M2KV-GV4k[youtube]

== Logical diagram
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/eda-ld.png[alt="An event driven architecture is made up of an event source, message broker, microservices, and automation orchestration", width=700]


== The technology
The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/management/ansible?intcmp=7013a00000318EWAAY[*Red Hat Ansible Automation Platform*] is used for adding a powerful layer of automation to a hybrid cloud environment. Through the form of playbooks, Ansible can deliver updates to large quantities of systems simultaneously, delivering consistent, reliable, and rapid response to any events triggered by this solution. https://www.redhat.com/en/technologies/management/ansible/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://access.redhat.com/products/red-hat-amq?intcmp=7013a00000318EWAAY[*Red Hat AMQ*] is a lightweight, high-performance, robust messaging platform. Here, it communicates between the event sources, all of the microservices that handle the events, and the automation layer that performs the final remediation, including results listening and response. AMQ provides efficient queuing and event streaming for seamless data exchange between applications and microservices, with extremely high throughput, and extremely low latency. 

https://www.redhat.com/en/technologies/jboss-middleware/fuse?intcmp=7013a00000318EWAAY[*Red Hat Fuse*] enables collaborative, agile building of applications using microservices and containers. Since aggregation is necessary for an Event Driven solution such as this, it’s even more powerful as Fuse provides this aggregation out of the box. 

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy. Here, it provides a consistent application platform to manage hybrid cloud, multicloud, and edge deployments. https://www.redhat.com/en/technologies/cloud-computing/openshift/ocp-self-managed-trial?intcmp=7013a000003Sh3TAAS[*Try It >*]
====

== Architectures

=== Event Driven Automation (network)
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/eda-sd-net.png[alt="A simple network mapping between the managed infrastructure, event source and the automated solution is required for efficient communication in an event driven architecture", width=700]
--

In this simplified network diagram, you’ll see that each component is broken down into their own communication channel, where we can define them as dark blue being the Managed Infrastructure, yellow being for Administration, and light blue being an Internal Network for the containers running the application services and routing environment. This is customizable to however fits your needs as long as the components in each channel are able to communicate as depicted above.



=== Event Driven Automation (data)
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/eda-sd-data.png[alt="An event driven architecture has a simple data flow from an event source through to the automation orchestrator, while updating status with third-party ticketing systems throughout the process", width=700]
--
1. An event source finds an anomaly and sends message(s) to the broker for what event has occurred.
2. The message broker queues the incoming messages, and sends a message out to the system event service.
3. The event response is then routed through the decision management logic, and a response is determined.
4. This response is then messaged to the create task topic
5. A task message is then processed triggering the task service.
6. Here the task service then creates a service ticket and routes through the task store, where updates will be incrementally added as the event continues through the event chain.
7. Simultaneous with the task topic creation, we can see we’ve also invoked the automation topic.
8. An automation message is then processed, triggering the automation service.
9. This then updates the execution store which, as mentioned earlier, flows back through the system event service, and subsequently updates with the execution status.
10. This service then sends a job to the Automation Platform.
11. Red Hat Ansible executes the job (via playbook) on all applicable hosts.
12. Results are returned on the same channel to Ansible…
13. Those results are sent along to the automation service that the job was just received from.
14. Simultaneously, the results are also sent back to a message broker.
15. The results messages are then processed and trigger the automation results service.
16. And that results service finally processes the updated results back through the same chain as earlier.



== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/event-driven-automation.drawio[[Open Diagrams]]
--

== Provide feedback 
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/event-driven-automation.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
